<?php
// Version: 2.0; Settings

// Important! Before editing these language files please read the text at the top of index.english.php.

global $settings;

$txt['theme_thumbnail_href'] = $settings['images_url'] . '/thumbnail.gif';
$txt['theme_description'] = 'LimeStone is a crisp, flat and responsive SMF theme.<br /><br />LimeStone by <a href="https://www.idesignsmf.com">idesignSMF</a>';

?>